const _ = require('lodash')
const { mongoose } = require('./../Db/mongoose')
const { ObjectID } = require('mongodb')

// Models
const Course = require('./../Model/Course')

// GET /courses
var get_all_courses = (req, res) => {
	Course.find({ }).then((courses) => {
		res.send(courses)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// POST /courses
var create_new_course = (req, res) => {
	let body = _.pick(req.body, ['id', 'name', 'credit', 'location'])
	let course = new Course(body)

	course.save().then((course) => {
		res.send(course)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// GET /courses/:courseId
var get_course = (req, res) => {
	Course.findOne({
		id: req.params.courseId
	}).then((course) => {
		if (!course){
			return res.status(404).send("Course not found")
		}

		res.send(course)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// PATCH /courses/:courseId
var modify_course = (req, res) => {
	Course.findOneAndUpdate({ 
		id: req.params.courseId
	}, req.body, {
		new: true
	}).then((course) => {
		if (!course) {
			return res.status(404).send("Course not found")
		}
		
		res.send(course)
	}).catch((e) => {
		console.log('You cannot modify this')
		res.status(400).send(e)
	})
}

// DELETE /courses/:courseId
var delete_course = (req, res) => {
	Course.findOneAndRemove({
		id: req.params.courseId
	}).then((course) => {
		if (!course) {
			return res.status(400).send("Course not found")
		}

		res.send(course)
	}).catch((e) => {
		res.status(400).send(e)
	})
}

// Exports
module.exports = {
	get_all_courses,
	create_new_course,
	get_course,
	modify_course,
	delete_course,
}