const mongoose = require('mongoose')

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost:27017/CircleEducation', {
	useNewUrlParser: true
})

module.exports = { mongoose }