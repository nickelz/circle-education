const mongoose = require('mongoose')

var CourseSchema = new mongoose.Schema({
	id: {
		type: String,
		minlength: 3,
		required: true
	},

	name: {
		type: String,
		required: true,
		minlength: 3
	},

	credit: {
		type: Number,
		required: true
	},

	location: {
		type: String,
		required: true
	},

	instructors: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Instructor',
		required: true
	}]
})

CourseSchema.pre('findOneAndUpdate', function (next) {
	let newId = this.getUpdate().id
	if (newId) {
		this.update({}, {
			$set: {
				id: sanitize(newId)
			}
		})
	}
	next()
})

module.exports = mongoose.model('Course', CourseSchema)