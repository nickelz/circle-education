const mongoose = require('mongoose')

var InstructorSchema = new mongoose.Schema({
	first_name: {
		type: String,
		minlength: 2,
		required: true
	},

	last_name: {
		type: String,
		minlength: 2,
		required: true
	},

	subject: {
		type: String,
		required: true,
		minlength: 2
	}
})

module.exports = mongoose.model('Instructor', InstructorSchema)