const mongoose = require('mongoose')

var StudentSchema = new mongoose.Schema({
	first_name: {
		type: String,
		minlength: 2,
		required: true
	},

	last_name: {
		type: String,
		minlength: 2,
		required: true
	},

	stdId: {
		type: Number,
		minlength: 2,
		required: true
	},

	registered: {
		type: Date,
		default: Date.now
		required: true
	}

	courses: [{
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Course',
		required: true
	}]
})

module.exports = mongoose.model('Student', StudentSchema)