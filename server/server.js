const express = require('express')
const bodyParser = require('body-parser')

// Controllers
const {
	get_all_courses,	// GET /courses
	create_new_course,	// POST /courses
	get_course, 		// GET /courses/:courseId
	modify_course,		// PATCH /courses/:courseId
	delete_course,		// DELETE /courses/:courseId
} = require('./Controller/CoursesController')

// Variables
let app = express()

// Configuration
app.use(bodyParser.json())

// Routing
app.route('/courses')
	.get(get_all_courses)
	.post(create_new_course)

app.route('/courses/:courseId')
	.get(get_course)
	.patch(modify_course)
	.delete(delete_course)

app.listen(3000, () => console.log('Server running on port 3000 ..'))